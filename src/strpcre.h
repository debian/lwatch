/* srcpcre.h -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#ifndef _ARC_STRPCRE_H
#define _ARC_STRPCRE_H

#include <pcre.h>

#define PCRE_COPT PCRE_CASELESS|PCRE_DOTALL
#define PARSELINE "^(.+?\\s.+?\\s.+?\\s)(.+?\\s)(.+?:\\s)(.*)$"
#define RE_NMATCHES 30

#define freestr(a) free((void*)a); a=NULL

void init_parser(void);
void free_parser(void);
int compile_re(char *re, pcre **pre, pcre_extra **prh);
void free_re(pcre **pre, pcre_extra **prh);
char *newstr(const char *str);
char *addstr(char *s1, const char *s2);
void showline(char *str);

#endif
