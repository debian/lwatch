%top{
#include "config.h"
}
%{
/* yparse.l -- LogWatcher

   This file is part of the LogWatcher tool.

   Copyright (C) 2002-2009 Artur Robert Czechowski

   The LogWatcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License, or (at your option) any later version.

   The LogWatcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with the LogWatcher; see the file COPYING.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Artur R. Czechowski
   <arturcz@hell.pl>
   http://hell.pl/arturcz/
 */

#include <stdio.h>
#include <string.h>

#include "acolors.h"
#include "control.h"
#include "log.h"
#include "settings.h"
#include "strpcre.h"

#define MAXMSG 80

int *yyip;
char *yypch;
char **yyppch;
int lineno=1;

char *restr;

#define STACKSIZE 5
int s_stack[STACKSIZE+1];
int s_pointer=0;
int c_state;

#define PUSH(a) if(s_pointer<STACKSIZE) { \
	s_stack[s_pointer++]=YY_START; \
	BEGIN(a); \
} else { \
	die("yparse.l: internal stack overflow\n"); \
}

#define POP if(s_pointer>0) { \
	BEGIN(s_stack[--s_pointer]); \
} else { \
	die("yparse.l: internal stack empty\n"); \
}

%}

%option noyywrap nounput noinput

M_KEYWORD	input_file|output_file|show_unparsed|cfg_ver|use_syslog|log_level|rule_action
R_KEYCOLOR	date_color|host_color|serv_color|mesg_color|color|highlight
R_KEYWORD	match_service|match_host|ignore|continue|exit
RA_KEYWORD	continue|exit
COLOR		black|red|green|brown|blue|magenta|cyan|lightgray|darkgray|brightred|brightgreen|yellow|brightblue|purple|brightcyan|white
LOG_LEVEL   LOG_EMERG|LOG_ALERT|LOG_CRIT|LOG_ERR|LOG_WARNING|LOG_NOTICE|LOG_INFO|LOG_DEBUG
STRING		".*"
VALUE		[0-9]*
COMMENT		#.*
RETOKEN		|\/
TEXT		[a-zA-Z_0-9/.-]*
BOOL		yes|no
INTVAL		[0-9]*

%x COLOR REGEXP ERROR SECOND ACTION MULTI TEXT BOOLEAN INTEGER RULEACTION LOGLEVEL

%%
%{
	char errmsg[MAXMSG+1];
	int errind=0;
#ifdef DEBUG
	char mymsg[MAXMSG+1];
#endif
	struct s_action action;
%}

<ERROR>.	if(errind<MAXMSG) errmsg[errind++]=*yytext;
<ERROR>\n	{
	errmsg[errind]='\0';
	die("Unknown configuration option in line %i:\n%s\n",lineno,errmsg);
}	
<*>[ \t]	;	/* Skip whitespaces */
<INITIAL,COLOR,REGEXP,ERROR,SECOND,MULTI>\n		lineno++;
<*>{COMMENT}	;	/* Skip comments */
<INITIAL>input_file=	{
	PUSH(TEXT);
	yyppch=&lw_conf.in_file;
#ifdef DEBUG
	strcpy(mymsg,"input_file");
#endif
}
<INITIAL>output_file=	{
	PUSH(TEXT);
	yyppch=&lw_conf.out_file;
#ifdef DEBUG
	strcpy(mymsg,"output_file");
#endif
}
<INITIAL>rule_action=	{
	PUSH(RULEACTION);
	yyip=&lw_conf.def_action_rule;
#ifdef DEBUG
	strcpy(mymsg,"rule_action");
#endif

}
<INITIAL>show_unparsed=	{
	PUSH(BOOLEAN);
	yyip=&lw_conf.show_unparsed;
#ifdef DEBUG
	strcpy(mymsg,"show_unparsed");
#endif
}
<INITIAL>cfg_ver= {
	PUSH(INTEGER);
	yyip=&lw_conf.cfg_ver;
#ifdef DEBUG
	strcpy(mymsg,"cfg_ver");
#endif
}
<INITIAL>use_syslog=	{
	PUSH(BOOLEAN);
	yyip=&lw_conf.use_syslog;
#ifdef DEBUG
	strcpy(mymsg,"use_syslog");
#endif
}
<INITIAL>log_level=	{
	PUSH(LOGLEVEL)
	yyip=&lw_conf.log_level;
#ifdef DEBUG
	strcpy(mymsg,"log_level");
#endif
}
<INITIAL>date_color=	{
	PUSH(COLOR);
	yyip=&lw_conf.def_date_color;
#ifdef DEBUG
	strcpy(mymsg,"date_color");
#endif
}
<INITIAL>host_color=	{
	PUSH(COLOR);
	yyip=&lw_conf.def_host_color;
#ifdef DEBUG
	strcpy(mymsg,"host_color");
#endif
}
<INITIAL>serv_color=	{
	PUSH(COLOR);
	yyip=&lw_conf.def_serv_color;
#ifdef DEBUG
	strcpy(mymsg,"serv_color");
#endif
}
<INITIAL>color=		|
<INITIAL>mesg_color=	{
	PUSH(COLOR);
	yyip=&lw_conf.def_mesg_color;
#ifdef DEBUG
	strcpy(mymsg,"mesg_color");
#endif
}
<MULTI,ACTION>date_color=	{
	PUSH(COLOR);
	yyip=&action.date_color;
#ifdef DEBUG
	strcpy(mymsg,"date_color");
#endif
}
<MULTI,ACTION>host_color=	{
	PUSH(COLOR);
	yyip=&action.host_color;
#ifdef DEBUG
	strcpy(mymsg,"host_color");
#endif
}
<MULTI,ACTION>serv_color=	{
	PUSH(COLOR);
	yyip=&action.serv_color;
#ifdef DEBUG
	strcpy(mymsg,"serv_color");
#endif
}
<MULTI,ACTION>color=		|
<MULTI,ACTION>mesg_color=	{
	PUSH(COLOR);
	yyip=&action.mesg_color;
#ifdef DEBUG
	strcpy(mymsg,"mesg_color");
#endif
}
<MULTI,ACTION>highlight=	{
	PUSH(COLOR);
	yyip=&action.highlight_color;
#ifdef DEBUG
	strcpy(mymsg,"highlight_color");
#endif
}
<COLOR>{COLOR}		{
	getvalbystr(colstr,yytext,*yyip);
	POP;
#ifdef DEBUG
	fprintf(stderr,"Set %s to %s[%i]\n",mymsg,yytext,*yyip);
#endif
}
<LOGLEVEL>{LOG_LEVEL}    {
    getvalbystr(loglevelstr,yytext,*yyip);
    POP;
#ifdef DEBUG
	fprintf(stderr,"Set %s to %s[%i]\n",mymsg,yytext,*yyip);
#endif
}
<TEXT>{TEXT} {
	if(!*yyppch) {
		*yyppch=strdup(yytext);
#ifdef DEBUG
		fprintf(stderr,"Set %s to %s\n",mymsg,yytext);
#endif
	}
	POP;
}
<BOOLEAN>{BOOL} {
	*yyip=0;
	if(!strcasecmp(yytext,"yes")) {
		*yyip=1;
	}
#ifdef DEBUG
	fprintf(stderr,"Set %s to %s (%i)\n",mymsg,yytext,*yyip);
#endif
	POP;
}
<RULEACTION>{RA_KEYWORD} {
	if(!strcasecmp(yytext,"exit")) {
		*yyip=RULE_EXIT;
	} else {
		*yyip=RULE_CONTINUE;
	}
#ifdef DEBUG
	fprintf(stderr,"Set %s to %s (%i)\n",mymsg,yytext,*yyip);
#endif
	POP;
}
<INTEGER>{INTVAL} {
	*yyip=0;
	*yyip=strtol(yytext, &yypch, 10);
	if(*yypch!='\0') {
	  die("Error in line %i: '%s' is not valid integer variable.\n",lineno,yytext);
	}
#ifdef DEBUG
	fprintf(stderr,"Set %s to %s (%i)\n",mymsg,yytext,*yyip);
#endif
	POP;
}
<MULTI,ACTION>match_service {
	if(action.match_host) {
	  die("Error in line %i: only one of match_service and match_host can be set.\n",lineno);
	}
	action.match_service=1;
#ifdef DEBUG
	fprintf(stderr,"Match service instead message\n");
#endif
}
<MULTI,ACTION>match_host {
	action.match_host=1;
	if(action.match_service) {
	  die("Error in line %i: only one of match_service and match_host can be set.\n",lineno);
	}
#ifdef DEBUG
	fprintf(stderr,"Match hostname instead message\n");
#endif
}
<MULTI,ACTION>ignore {
	action.ignore=1;
	action.action=RULE_EXIT;
#ifdef DEBUG
	fprintf(stderr,"Ignore RE\n");
#endif
}
<MULTI,ACTION>exit {
	action.action=RULE_EXIT;
#ifdef DEBUG
	fprintf(stderr,"Exit on action\n");
#endif
}
<MULTI,ACTION>continue {
	if(action.ignore==1) {
#ifdef DEBUG
		fprintf(stderr,"This rule is for ignoring the pattern. Using continue makes no sense");
#endif
	} else {
		action.action=RULE_CONTINUE;
#ifdef DEBUG
		fprintf(stderr,"Continue with the next action\n");
#endif
	}
}
<INITIAL>\/	{
	BEGIN(SECOND);
	yyless(0);
}
<SECOND>\/	{
	memset(&action,0,sizeof(action));
	BEGIN(REGEXP);
}
<REGEXP>\/ 	{
	BEGIN(ACTION);
}
<REGEXP>.*/\/ {
	restr=newstr(yytext);
#ifdef DEBUG
	action.restr=restr;
#endif
	compile_re(restr,&action.re,&action.rh);
}
<ACTION>\n	{
	lineno++;
	add_action(&action);
#ifndef DEBUG
	freestr(restr);
#endif
	BEGIN(SECOND);
}
<ACTION>\{	BEGIN(MULTI);
<MULTI>\}	{
	add_action(&action);
#ifndef DEBUG
	freestr(restr);
#endif
	BEGIN(SECOND);
}
<*>.			{
	if(errind<MAXMSG) errmsg[errind++]=*yytext;
	BEGIN(ERROR);
}	

%%
