;;
;; aterm with lwatch inside in background
;;
;; Jakub Turski <yacoob(at)hell.pl>
;;
;; DESCRIPTION:
;;
;; runs semi-transparent aterm in the background, with lwatch inside


;; SETTINGS
;; the name of the xterm (-T option)
(define lwatch-name "__lwatch__")
;; the command to execute
(define lwatch-program (concat "aterm +sb -geometry 120x48+30+60 -tr -sh 60 -T " lwatch-name " -e lwatch"))
;; delay to wait after initial launch to hide the window
(define lwatch-delay 125)


;; OTHER VARS
;; actual lwatch window
(define lwatch-handle nil)
(define lwatch-xid nil)
;; is it down, or up?
(define lwatch-in #f)

(defun lwatch-grab-window (w)
	(when (= (window-name w) lwatch-name)
		(setq lwatch-handle w)
		(setq lwatch-xid (window-id w))
		(set-window-depth w -16)
		(window-put w 'sticky t)
		(window-put w 'group 'desk)
		(window-put w 'cycle-skip t)
		(window-put w 'taskbar-skip t)
		(window-put w 'window-list-skip t)
		(window-put w 'window-locked-vertically t)
		(window-put w 'window-locked-horizontally t)
		(window-put w 'never-focus t)
		(set-window-frame w ())
	)
)


;; fade in fade out
(define (toggle-lwatch)
	(catch 'return
		;; check if the window is there, if not - spawn it
		(when (or (not lwatch-xid) (not (get-window-by-id lwatch-xid)))
			(system (concat lwatch-program " &"))
			(setq lwatch-in #t)
			(throw 'return nil)
		)
	
		;; pull console up or roll it down
		(if lwatch-in
			;; hide it
			(iconify-window lwatch-handle)
			;; show it
			(uniconify-window lwatch-handle)
		)
	
		;; negate the flag
		(setq lwatch-in (not lwatch-in))
	)
)


;; make it a command available for binding
(define-command 'toggle-lwatch toggle-lwatch)

;; put a hook to catch newly spawned  window
(add-hook 'after-add-window-hook lwatch-grab-window)

;; actually run the command
(toggle-lwatch)
(make-timer
	(lambda ()
	  (toggle-lwatch)
	) 0 lwatch-delay)

;; voila!
(provide 'lwatch)
