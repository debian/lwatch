0.6.2:  - updated config.{sub,guess} - allows build for aarch64 Linux.

0.6.1:  - quit configure with error when pgk-config is not found

0.6:    - it compiles under Debian GNU/Hurd now
        - typos in documentation fixed
        - some updates in README - removed obviously obsoleted information
        - provide packaging/lwatch.spec to build packages for rpm based
          distributions, thanks to Soeren (SF#2945844)
        - revert change made in 0.4 - now we do not create default input_file
          during make install, this shall be done by the user, administrator
          or packager
        - syslog support
        - can be run as a daemon

0.5:    - permanently remove input_fifo, --fifo and -f
        - dumping ./configure flags when run with -v
        - added contrib/lwatch.jl for sawfish, thanks to Jakub Turski
        - order of rules is important, note in lwatch.conf(5) (sf#910332)
        - purge support for cutting line to fit in width in resized xterm,
          current patch is broken, personally I think the feature is
          superfluous, however I would be glad to accept a working patch
          if you find this functionality vital for you
        - cfg_ver is obligatory now, consult lwatch.conf(5)  
        - default value for exit/continue on matching rule is configurable now,
          see lwatch.conf(5) for details, thanks to Julien Sterckeman
          for idea
        - updated config.{sub,guess}
        - updated autotools

0.4.1:    - manual typos (arturcz)
        - change defaults in config file (arturcz)
        - minor changes needed to build on OpenBSD, thanks pelotas (arturcz)
        - fixed lwatch crashes for some spurious configuration, thanks kender
          (arturcz)

0.4:    - cleaned code to compile without warnings when -Wall is used,
          at least on Linux/i386 (arturcz)
        - input_fifo is obsoleted now, use input_file instead (arturcz)
        - fixed SEGV when run with -O and without -o (arturcz)
        - --enable-resize allows to cut line length to width of terminal,
          enabling this option could causes your lwatch to crash (oszer)
        - versioning of configuration file, throw warning if config file is
          without version (arturcz)
        - complete user manual (arturcz)
        - complete build documentation: README, README.cvs, INSTALL (arturcz)
        - configure: --with-pcre support (arturcz)
        - create default input_file during make install (arturcz)

0.3:    - --fifo and -f are deprecated, use --input and -i instead (arturcz)
        - new options for output file (-o and --output) (arturcz)
        - -o means output file, use -O for omit config file (arturcz)
        - fifo_file in configuration file is deprecated, use input_file
          instead it (arturcz)
        - new option output_file in configuration (arturcz)
        - new tests in ./configure for getopt_long (arturcz)
        - handle EOF - reopen fifo, close when input from stdin (arturcz)
        - colors with BOLD atribute available (arturcz)
        - FreeBSD port (arturcz)

0.2:    - bugfix: too much manual files installed (arturcz)
        - ChangeLog filled :) (arturcz)
        - reading lwatch.conf from $sysconfdir (arturcz)
        - compliant with GPL2.0 (arturcz)
        - support for show_unparsed (arturcz)
        - bugfix: yes and no was negated (arturcz)
        - some typos corrected (arturcz)
        - built-in default for input file could be replaced during build
          time (arturcz)
        - legal stuff required by PCRE (arturcz)

0.1:    - first public release. Basic functionality implemented (parse and
          colourize logs) (arturcz)

# vim:tw=78:
